<?php
  class Jugador extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un jugador en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("jugador",
                $datos);
    }
    //FUNCION PARA CONSULTAR
    function obtenerTodos(){
      $listadoJugador=
      $this->db->get("jugador");
      if($listadoJugador
          ->num_rows()>0){//si hay tados
            return $listadoJugador->result();

          }else{
            return false;
          }
    }
    //borrar
    function borrar($id_jug){
      $this->db->where("id_jug",$id_jug);
      return $this->db->delete("jugador");
    }
  }//Cierre de la clase

 ?>
