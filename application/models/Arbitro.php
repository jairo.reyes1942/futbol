<?php
  class Arbitro extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un arbitro en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("arbitro",
                $datos);
    }
    //FUNCION PARA CONSULTAR
    function obtenerTodos(){
      $listadoArbitros=
      $this->db->get("arbitro");
      if($listadoArbitros
          ->num_rows()>0){//si hay tados
            return $listadoArbitros->result();

          }else{
            return false;
          }
    }
    //borrar
    function borrar($id_arb){
      $this->db->where("id_arb",$id_arb);
      return $this->db->delete("arbitro");
    }
  }//Cierre de la clase

 ?>
