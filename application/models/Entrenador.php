<?php
  class Entrenador extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un entrenador en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("entrenador",
                $datos);
    }
    //FUNCION PARA CONSULTAR
    function obtenerTodos(){
      $listadoEntrenador=
      $this->db->get("entrenador");
      if($listadoEntrenador
          ->num_rows()>0){//si hay tados
            return $listadoEntrenador->result();

          }else{
            return false;
          }
    }
    //borrar
    function borrar($id_ent){
      $this->db->where("id_ent",$id_ent);
      return $this->db->delete("entrenador");
    }
  }//Cierre de la clase

 ?>
