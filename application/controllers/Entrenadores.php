<?php
    class Entrenadores extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //cargar todos los modelos que necesitemos
        $this->load->model('Entrenador');
      }
//Funcion que renderiza la vista index
      public function index(){
        $data['entrenadores']=$this->Entrenador->ObtenerTodos();
        $this->load->view('header');
        $this->load->view('entrenadores/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('entrenadores/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoEntrenador=array(
          "cedula_ent"=>$this->input->post('cedula_ent'),
          "nombre_ent"=>$this->input->post('nombre_ent'),
          "apellido_ent"=>$this->input->post('apellido_ent'),
          "telefono_ent"=>$this->input->post('telefono_ent'),
          "email_ent"=>$this->input->post('email_ent'),
          "direccion_ent"=>$this->input->post('direccion_ent')
        );
        //imprime los datos del array que creamos
        //print_r($datosNuevoEntrenador);
        if($this->Entrenador->Insertar($datosNuevoEntrenador)){
          redirect('entrenadores/index');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }
      }
      //funcion para eliminar entrenadores
      public function eliminar($id_ent){

        if ($this->Entrenador->borrar($id_ent)) {
          redirect('entrenadores/index');
        } else {

        }

      }
    }//Ciere de la clase
  ?>
