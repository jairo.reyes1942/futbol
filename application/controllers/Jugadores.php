<?php
    class Jugadores extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //cargar todos los modelos que necesitemos
        $this->load->model('Jugador');
      }
//Funcion que renderiza la vista index
      public function index(){
        $data['jugadores']=$this->Jugador->ObtenerTodos();
        $this->load->view('header');
        $this->load->view('jugadores/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('jugadores/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoJugador=array(
          "cedula_jug"=>$this->input->post('cedula_jug'),
          "nombre_jug"=>$this->input->post('nombre_jug'),
          "apellido_jug"=>$this->input->post('apellido_jug'),
          "telefono_jug"=>$this->input->post('telefono_jug'),
          "email_jug"=>$this->input->post('email_jug'),
          "direccion_jug"=>$this->input->post('direccion_jug')
        );
        //imprime los datos del array que creamos
        //print_r($datosNuevojugador);
        if($this->Jugador->Insertar($datosNuevoJugador)){
          redirect('jugadores/index');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }
      }
      //funcion para eliminar jugadores
      public function eliminar($id_jug){

        if ($this->Jugador->borrar($id_jug)) {
          redirect('jugadores/index');
        } else {

        }

      }
    }//Ciere de la clase
  ?>
