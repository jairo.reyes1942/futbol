<?php
    class Arbitros extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //cargar todos los modelos que necesitemos
        $this->load->model('Arbitro');
      }
//Funcion que renderiza la vista index
      public function index(){
        $data['arbitros']=$this->Arbitro->ObtenerTodos();
        $this->load->view('header');
        $this->load->view('arbitros/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('arbitros/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoArbitro=array(
          "cedula_arb"=>$this->input->post('cedula_arb'),
          "nombre_arb"=>$this->input->post('nombre_arb'),
          "apellido_arb"=>$this->input->post('apellido_arb'),
          "nacionalidad_arb"=>$this->input->post('nacionalidad_arb'),
          "telefono_arb"=>$this->input->post('telefono_arb'),
          "email_arb"=>$this->input->post('email_arb'),
          "direccion_arb"=>$this->input->post('direccion_arb')
        );
        //imprime los datos del array que creamos
        //print_r($datosNuevoArbitror);
        if($this->Arbitro->Insertar($datosNuevoArbitro)){
          redirect('arbitros/index');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }
      }
      //funcion para eliminar earbitros
      public function eliminar($id_arb){

        if ($this->Arbitro->borrar($id_arb)) {
          redirect('arbitros/index');
        } else {

        }

      }
    }//Ciere de la clase
  ?>
