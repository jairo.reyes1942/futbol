<center><h1 style="color:blue">NUEVO JUGADOR</h1></center>
<form class="table-bordered" class="" action="<?php echo site_url(); ?>/jugadores/guardar" method="post">
<br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_jug" value="" id="cedula_jug">
      </div>
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_jug" value="" id="nombre_jug">
      </div>
      <div class="col-md-4">
        <label for="">Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el apellido"
        class="form-control"
        name="apellido_jug" value="" id="apellido_jug">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Telefono:</label>
          <br>
          <input type="number"
          placeholder="Ingrese el telefono"
          class="form-control"
          name="telefono_jug" value="" id="telefono_jug">
      </div>
      <div class="col-md-4">
        <label for="">Email:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el email"
        class="form-control"
        name="email_jug" value="" id="email_jug">
      </div>
      <div class="col-md-4">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_jug" value="" id="direccion_jug">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/jugadores/index" class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
    <br>
</form>
