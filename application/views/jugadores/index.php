<center><h1 style="color:blue">LISTADO DE JUGADORES</h1></center>
<br>
<?php if ($jugadores): ?>
 <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>CEDULA</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>TELEFONO</th>
        <th>EMAIL</th>
        <th>DIRECCION</th>
        <th>ACCIONES</th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($jugadores as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_jug ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_jug ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_jug ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_jug ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_jug ?>
          </td>
          <td>
            <?php echo $filaTemporal->email_jug ?>
          </td>
          <td>
            <?php echo $filaTemporal->direccion_jug ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Jugador">
              <i style="color:orange" class="glyphicon glyphicon-edit"></i>
            </a>
              &nbsp;   &nbsp;   &nbsp;
            <a href="<?php echo site_url(); ?>/jugadores/eliminar/<?php echo $filaTemporal->id_jug ?>" title="Eliminar Jugador">
              <i style="color:red" class="glyphicon glyphicon-trash"></i>
            </a>
          </td>

        </tr>
      <?php endforeach; ?>
    </tbody>
 </table>
<?php else: ?>
  <center><h1 style="color:red"><b>No existen Jugadores por favor registre un Jugador nuevo</b></h1></center>
<?php endif; ?>
