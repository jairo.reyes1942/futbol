<center><h1 style="color:blue">NUEVO ARBITRO</h1></center>
<form class="table-bordered" class="" action="<?php echo site_url(); ?>/arbitros/guardar" method="post">
<br>
<br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_arb" value="" id="cedula_arb">
      </div>
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_arb" value="" id="nombre_arb">
      </div>
      <div class="col-md-4">
        <label for="">Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el apellido"
        class="form-control"
        name="apellido_arb" value="" id="apellido_arb">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nacionalidad:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la nacionalidad"
          class="form-control"
          name="nacionalidad_arb" value="" id="nacionalidad_arb">
      </div>
      <div class="col-md-4">
          <label for="">Telefono:</label>
          <br>
          <input type="number"
          placeholder="Ingrese el telefono"
          class="form-control"
          name="telefono_arb" value="" id="telefono_arb">
      </div>
      <div class="col-md-4">
        <label for="">Email:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el email"
        class="form-control"
        name="email_arb" value="" id="email_arb">
      </div>

    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_arb" value="" id="direccion_arb">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/arbitros/index" class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
    <br>
    <br>
</form>
