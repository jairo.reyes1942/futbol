<center><h1 style="color:blue">LISTADO DE ARBITROS</h1></center>
<br>
<?php if ($arbitros): ?>
 <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>CEDULA</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>NACIONALIDAD</th>
        <th>TELEFONO</th>
        <th>EMAIL</th>
        <th>DIRECCION</th>
        <th>ACCIONES</th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($arbitros as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_arb ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_arb ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_arb ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_arb ?>
          </td>
          <td>
            <?php echo $filaTemporal->nacionalidad_arb ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_arb ?>
          </td>
          <td>
            <?php echo $filaTemporal->email_arb ?>
          </td>
          <td>
            <?php echo $filaTemporal->direccion_arb ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Arbitro">
              <i style="color:orange" class="glyphicon glyphicon-edit"></i>
            </a>
              &nbsp;   &nbsp;   &nbsp;
            <a href="<?php echo site_url(); ?>/arbitros/eliminar/<?php echo $filaTemporal->id_arb ?>" title="Eliminar Arbitro">
              <i style="color:red" class="glyphicon glyphicon-trash"></i>
            </a>
          </td>

        </tr>
      <?php endforeach; ?>
    </tbody>
 </table>
<?php else: ?>
  <center><h1 style="color:red"><b>No existen Arbitros por favor registre un Arbitros nuevo</b></h1></center>
<?php endif; ?>
