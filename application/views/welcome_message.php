<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url(); ?>/assets/images/img.jpg" alt="imagen 1" style="width: 100%; height: 500px;">
    </div>

    <div class="item">
			<img src="<?php echo base_url(); ?>/assets/images/img2.jpg" alt="imagen 2" style="width: 100%; height: 500px;">
    </div>

    <div class="item">
			<img src="<?php echo base_url(); ?>/assets/images/img3.jpg" alt="imagen 3" style="width: 100%; height: 500px;">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>
<br>
<img src="<?php echo base_url(); ?>/assets/images/img4.jpg" alt="imagen 1" style="width: 100%; height: 500px;">
<div class="row">
	<div class="col-md-12">
		<div class="col-md-1">
		</div>
		<div class="col-md-10">
    <h2 style="text-align: justify;">Las escuelas de fútbol están orientadas hacia el desarrollo completo de las capacidades y habilidades de los jugadores. Estas tratan de mejorar sus cualidades volitivas tan fundamentales en los deportes de equipo, tales como la voluntad, el esfuerzo y la cooperación.</h2>
  	</div>
		<div class="col-md-1">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-1">
		</div>
		<div class="col-md-10">
      <br>
      <br>
      <center><h2 style="color:black"><b>FRASES DE JUGADORES FAMOSOS</b></h2></center>
  	</div>
		<div class="col-md-1">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-1">
		</div>
		<div class="col-md-10" >
      <center><h2 style="text-align: justify;" class="table table-bordered" >Si muero un día, estoy feliz porque traté de dar lo mejor de mí. Mi deporte me permitió hacer mucho, porque es el deporte más grande del mundo. "Pelé" </h2></center>
  	</div>
		<div class="col-md-1">
		</div>
	</div>
</div>
<br>
<br>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-1">
		</div>
		<div class="col-md-10" >
      <center><h2 style="text-align: justify;" class="table table-bordered" >No tengo tiempo para pasatiempos. Al final del día, trato mi trabajo como un hobby. Es algo que me encanta hacer. "David Beckham"</h2></center>
  	</div>
		<div class="col-md-1">
		</div>
	</div>
</div>
<br>
<br>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-1">
		</div>
		<div class="col-md-10" >
      <center><h2 style="text-align: justify;" class="table table-bordered" >Una vez lloré porque no tenía zapatos para jugar fútbol, ​​pero un día, conocí a un hombre que no tenía pies. "Zinedine Zidane"</h2></center>
  	</div>
		<div class="col-md-1">
		</div>
	</div>
</div>
<br>
<br>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-1">
		</div>
		<div class="col-md-10" >
      <center><h2 style="text-align: justify;" class="table table-bordered" >No podemos mirarnos siempre en el espejo y decir lo buenos que somos. Cuando las cosas van bien es cuando hay que estar más atentos. El miedo a perder es la razón fundamental para competir bien. "Pep Guardiola"</h2></center>
  	</div>
		<div class="col-md-1">
		</div>
	</div>
</div>
