<center><h1 style="color:blue">NUEVO ENTRENADOR</h1></center>
<form class="table-bordered" class="" action="<?php echo site_url(); ?>/entrenadores/guardar" method="post">
<br>
<br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_ent" value="" id="cedula_ent">
      </div>
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_ent" value="" id="nombre_ent">
      </div>
      <div class="col-md-4">
        <label for="">Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el apellido"
        class="form-control"
        name="apellido_ent" value="" id="apellido_ent">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Telefono:</label>
          <br>
          <input type="number"
          placeholder="Ingrese el telefono"
          class="form-control"
          name="telefono_ent" value="" id="telefono_ent">
      </div>
      <div class="col-md-4">
        <label for="">Email:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el email"
        class="form-control"
        name="email_ent" value="" id="email_ent">
      </div>
      <div class="col-md-4">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_ent" value="" id="direccion_ent">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/entrenadores/index" class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
    <br>
    <br>
</form>
