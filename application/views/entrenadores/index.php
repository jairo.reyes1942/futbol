<center><h1 style="color:blue">LISTADO DE ENTRENADORES</h1></center>
<br>
<?php if ($entrenadores): ?>
 <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>CEDULA</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>TELEFONO</th>
        <th>EMAIL</th>
        <th>DIRECCION</th>
        <th>ACCIONES</th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($entrenadores as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_ent ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_ent ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_ent ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_ent ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_ent ?>
          </td>
          <td>
            <?php echo $filaTemporal->email_ent ?>
          </td>
          <td>
            <?php echo $filaTemporal->direccion_ent ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Entrenador">
              <i style="color:orange" class="glyphicon glyphicon-edit"></i>
            </a>
              &nbsp;   &nbsp;   &nbsp;
            <a href="<?php echo site_url(); ?>/entrenadores/eliminar/<?php echo $filaTemporal->id_ent ?>" title="Eliminar Entrenador">
              <i style="color:red" class="glyphicon glyphicon-trash"></i>
            </a>
          </td>

        </tr>
      <?php endforeach; ?>
    </tbody>
 </table>
<?php else: ?>
  <center><h1 style="color:red"><b>No existen Entrenadores por favor registre un Entrenador nuevo</b></h1></center>
<?php endif; ?>
